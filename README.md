# Requirements

* Node.js
 
## Installation
Clone git repository:
```bash
git clone git@bitbucket.org:longsightedfilms/david-varga-site.git
```
Navigate to cloned directory and run in terminal:
```bash
gulp watch
```
### Components
* SASS compiler
* BrowserSync
* CSSNano (for minifying)
* Autoprefixer